#include "breakout.h"
#include <SDL2/SDL.h>
#include <stdlib.h>
#include <stdio.h>

#include "common.h"

#define SCREEN_WIDTH	48
#define SCREEN_HEIGHT	64

#define BLOCK_WIDTH	4
#define BLOCK_HEIGHT	2
#define BALL_WIDTH	1
#define BLOCK_OFFSET	24

#define BLOCK_LINES	8
#define BALL_SPEED_MUL	1
#define PADDLE_DRAG	2

#define PADDLE_INC	3

#define COL_NONE	0
#define COL_RED		1
#define COL_GREEN	2
#define COL_BLUE	3


struct breakout_screen {
	SDL_Window *win;
	SDL_Renderer *ren;
	SDL_Texture *buf_txt;
	int width;
	int height;
	uint8_t *buffer;
	int buffer_sz;
};

struct breakout *init_breakout(void) {
	struct breakout *brk = (struct breakout *)
				alloc(sizeof(struct breakout));
	struct breakout_screen *scr = (struct breakout_screen *)
				alloc(sizeof(struct breakout_screen));
	struct block *blck;
	brk->scr = scr;
	brk->game_over = 0;
	brk->nr_blocks = 0;
	blck = (struct block *)alloc(sizeof(struct block));
	blck->width = 2 * BLOCK_WIDTH;
	blck->height = BLOCK_HEIGHT / 2;
	blck->draw = 1;
	brk->blck = blck;

	for (int i = 0; i < BLOCK_LINES; i++) {
		for (int j = 0; j < (SCREEN_WIDTH / BLOCK_WIDTH); j++) {
			struct block *new =  (struct block *)alloc(
					sizeof(struct block));
			new->pos_y = BLOCK_OFFSET - i * BLOCK_HEIGHT;
			new->pos_x = j * BLOCK_WIDTH;
			new->width = BLOCK_WIDTH;
			new->height = BLOCK_HEIGHT;
			new->draw = 1;

			blck->next = new;
			blck = new;
			brk->nr_blocks++;
		}
	}

	scr->width = SCREEN_WIDTH;
	scr->height = SCREEN_HEIGHT;
	scr->buffer_sz = scr->width * scr->height * 3;
	scr->buffer = (uint8_t *)alloc(scr->buffer_sz * sizeof(uint8_t));
	memset(scr->buffer, 0, scr->buffer_sz);

	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
		printf("SDL_Init Error: %s\n", SDL_GetError());
		return NULL;
	}

	scr->win = SDL_CreateWindow("Breakout clone",
				SDL_WINDOWPOS_CENTERED,
				SDL_WINDOWPOS_CENTERED,
				scr->width,
				scr->height,
				SDL_WINDOW_SHOWN |
				SDL_WINDOW_RESIZABLE);
	if (!scr->win) {
		printf("SDL_CreateWindow Error: %s\n", SDL_GetError());
		return NULL;
	}

	scr->ren = SDL_CreateRenderer(scr->win, -1,
				SDL_RENDERER_ACCELERATED |
				SDL_RENDERER_PRESENTVSYNC);
	if (!scr->ren) {
		printf("SDL_CreateRenderer Error: %s\n", SDL_GetError());
		if (!scr->win)
			SDL_DestroyWindow(scr->win);
		SDL_Quit();
		return NULL;
	}

	scr->buf_txt = SDL_CreateTexture(scr->ren,
                           SDL_PIXELFORMAT_RGB24,
                           SDL_TEXTUREACCESS_STREAMING, 
                           scr->width,
                           scr->height);

	breakout_reset(brk);
	return brk;
}

int breakout_get_screen(struct breakout *brk, double **dst, int sz) {
	struct breakout_screen *scr = brk->scr;
	int len = (scr->buffer_sz / 3) > sz ? sz : scr->buffer_sz / 3;

	for (int i = 0; i < len; i++) {
		double val = 0;
		for (int j = 0; j < 3; j++)
			val = (scr->buffer[i * 3 + j]) ? j + 1 : val;
		(*dst)[i] = (double)val;
	}

	return 0;
}

int breakout_get_screen_sz(struct breakout *brk) {
	struct breakout_screen *scr = brk->scr;
	return scr->buffer_sz / 3;
};

int breakout_get_score(struct breakout *brk) {
	return brk->score;
}

void breakout_reset(struct breakout *brk) {
	struct block *blck = brk->blck;
	struct breakout_screen *scr = brk->scr;

	blck->pos_x = 0;	
	blck->pos_y = SCREEN_HEIGHT - BLOCK_HEIGHT;	
	while (blck) {
		struct block *next = blck->next;
		blck->draw = 1;
		blck = next;
	}
	
	brk->ball_pos_x = (SCREEN_WIDTH / 4);
	brk->ball_pos_y = (SCREEN_HEIGHT / 2);
	brk->ball_vel_x = -1;
	brk->ball_vel_y = 1;
	brk->game_over = 0;
	brk->score = 0;
	memset(scr->buffer, 0, scr->buffer_sz);
	breakout_run(brk);
}

int deinit_breakout(struct breakout *brk) {
	struct breakout_screen *scr = brk->scr;
	struct block *blck = brk->blck;
	SDL_DestroyTexture(scr->buf_txt);
	SDL_DestroyRenderer(scr->ren);
	SDL_DestroyWindow(scr->win);
	SDL_Quit();

	while (blck) {
		struct block *next = blck->next;
		release(blck);
		blck = next;
	}
	
	release(brk->scr);
	release(brk);
}

static void draw_block(struct breakout *brk, int color,
		int pos_x, int pos_y, int width, int height) {
	struct breakout_screen *scr = brk->scr;
	uint8_t *buff = scr->buffer;

	for (int y = pos_y; y < (pos_y + height); y++)
	for (int x = pos_x; x < (pos_x + width); x++) {
		int idx = (y * scr->width + x) * 3;
		for (int col = 0; col < 3; col++) {
			uint8_t val = (col == color - 1) ? 0xff : 0;
			buff[idx + col] = val;
		}
	}
}

static void draw_blocks(struct breakout *brk) {
	struct block *blck = brk->blck;
	int color = COL_GREEN;

	while(blck) {
		if (blck->draw) {
			draw_block(brk, color, blck->pos_x, blck->pos_y,
				blck->width, blck->height);
		}
		color = COL_BLUE;
		blck = blck->next;
	}
};


void change_paddle_pos(struct breakout *brk, int delta_x, int delta_y) {
	struct breakout_screen *scr = brk->scr;
	struct block *blck = brk->blck;
	int paddle_pos_x =blck->pos_x + delta_x * PADDLE_INC;
	int paddle_pos_y =blck->pos_y + delta_y * PADDLE_INC;

	if (paddle_pos_x > scr->width - blck->width)
		paddle_pos_x = scr->width - blck->width;
	if (paddle_pos_x < 0)
		paddle_pos_x = 0;

	if (paddle_pos_y < 0)
		paddle_pos_y = 0;
	if (paddle_pos_y > scr->height - blck->height)
		paddle_pos_y = scr->height - blck->height;

	brk->blck->pos_x = paddle_pos_x;	
	brk->blck->pos_y = paddle_pos_y;	
}

static void update_ball(struct breakout *brk, int paddle_vel_x) {
	struct block *blck = brk->blck;
	int vel_x = brk->ball_vel_x;
	int vel_y = brk->ball_vel_y;

	brk->ball_pos_x += BALL_SPEED_MUL * vel_x;
	brk->ball_pos_y += BALL_SPEED_MUL * vel_y;

	int ball_edge_x0 = brk->ball_pos_x + BALL_WIDTH;
	int ball_edge_x1 = brk->ball_pos_x;
	int ball_edge_y0 = brk->ball_pos_y + BALL_WIDTH;
	int ball_edge_y1 = brk->ball_pos_y;

	while (blck) {
		int col = 0;
		int blck_edge_x0 = blck->pos_x;
		int blck_edge_x1 = blck->pos_x + blck->width;
		int blck_edge_y0 = blck->pos_y;
		int blck_edge_y1 = blck->pos_y + blck->height;

		if (blck->draw && (((ball_edge_y1 == blck_edge_y1) ||
				(ball_edge_y0 == blck_edge_y0)) && 
				(((ball_edge_x1 <= blck_edge_x1) &&
				(ball_edge_x1 >= blck_edge_x0)) ||
				(((ball_edge_x0 <= blck_edge_x1) &&
				(ball_edge_x0 >= blck_edge_x0)))))) {
			col = 1;
			brk->ball_vel_y = (-1) * brk->ball_vel_y;
		} 

		if (blck->draw && (((ball_edge_x1 == blck_edge_x1) ||
				(ball_edge_x0 == blck_edge_x0)) && 
				(((ball_edge_y0 <= blck_edge_y0) &&
				(ball_edge_y0 >= blck_edge_y1)) ||
				(((ball_edge_y1 >= blck_edge_y0) &&
				(ball_edge_y1 <= blck_edge_y1)))))) {
			col = 1;
			brk->ball_vel_x = (-1) * brk->ball_vel_x;
		} 
		if (col) {
			if (blck != brk->blck) {
				blck->draw = 0;
				brk->score++;
				break;
			} else {
				brk->ball_vel_x += paddle_vel_x;
			}
		}
		blck = blck->next;
	}

	if (brk->ball_pos_x <= 0) {
		brk->ball_pos_x = 0;
		brk->ball_vel_x = (-1) * brk->ball_vel_x;
	}
	if (brk->ball_pos_x > (SCREEN_WIDTH - BALL_WIDTH)) {
		brk->ball_pos_x = SCREEN_WIDTH - BALL_WIDTH;
		brk->ball_vel_x = (-1) * brk->ball_vel_x;
	}
	if (brk->ball_pos_y <= 0) {
		brk->ball_pos_y = 0;
		brk->ball_vel_y = (-1) * brk->ball_vel_y;
	}

	if (brk->ball_pos_y >= SCREEN_HEIGHT -BALL_WIDTH)
		brk->game_over = 1;
}

int breakout_run(struct breakout *brk) {
	struct breakout_screen *scr = brk->scr;
	SDL_Event event;
	int ret = 1;
	int paddle_vel_x = 0, paddle_vel_y = 0;

	while (SDL_PollEvent(&event)) {
		if (event.type == SDL_QUIT)
			ret = -1;
		if (event.type == SDL_KEYDOWN) {
			if (brk->human_input) {
				if ((char)event.key.keysym.sym == 'a')
					paddle_vel_x += -1;
				if ((char)event.key.keysym.sym == 'd')
					paddle_vel_x += 1;
//				if ((char)event.key.keysym.sym == 'w')
//					paddle_vel_y = -1;
//				if ((char)event.key.keysym.sym == 's')
//					paddle_vel_y = 1;
				if ((char)event.key.keysym.sym == 'r')
					breakout_reset(brk);
				change_paddle_pos(brk, paddle_vel_x,
							paddle_vel_y);
			}
		}
	}

	if (brk->score == brk->nr_blocks)
		brk->game_over = 1;

	if (brk->game_over)
		return 0;

	update_ball(brk, paddle_vel_x);

	memset(scr->buffer, 0, scr->buffer_sz);
	draw_blocks(brk);
	draw_block(brk, COL_RED, brk->ball_pos_x, brk->ball_pos_y,
				BALL_WIDTH, BALL_WIDTH);

	SDL_UpdateTexture(scr->buf_txt, NULL, scr->buffer, scr->width * 3);
	SDL_RenderClear(scr->ren);
	SDL_RenderCopy(scr->ren, scr->buf_txt, NULL, NULL);
	SDL_RenderPresent(scr->ren);
	if (brk->human_input)
		SDL_Delay(1000 / 15);
	return ret;
};
