#ifndef __NEURAL_NET_H__
#define __NEURAL_NET_H__

/*
[Ii]	[Hj]
[I0]	[H0]	[Ok]
[I1]	[H1]	[O0]
[I2]	[H2]	[O1]
[I3]	[H3]	[O2]
...	...	...
[In]	[Hm]	[Ol]

Weights:
wH[j * n + i]
wO[k * m + j]

*/

struct layer {
	struct layer *next;
	struct layer *prev;
	int size;
	double *biases;
	double *weights;
	double *tmp; // store to avoid recalculation activation function
	double *deltas;
};

struct neural_net {
	int nr_inputs;
	int nr_outputs;
	int nr_hidden;
	int hidden_size;
	struct layer *input;
	struct layer *output;
	struct layer *hidden;
	double (*activ)(double);
	double (*d_activ)(double);
};

struct neural_net *init_neural_net(int nr_inputs, int nr_outputs,
				int nr_hidden, int hidden_size);
void deinit_neural_net(struct neural_net *net);

struct training_entry {
	double *inputs;
	double *outputs;
};

struct training_set {
	int size;
	struct training_entry *entries;
};

struct training_set *init_training_set(struct neural_net *net, int size);
void deinit_training_set(struct training_set *set);
void shuffle_array(int *array, int size);
struct training_entry *get_rand_entry(struct training_set *set);

void calc_activation(struct neural_net *net, double *inputs, int nr_inputs);
double *get_outputs(struct neural_net *net);
void calc_deltas(struct neural_net *net, double *outputs);
void apply_deltas(struct neural_net *net, double learning_rate);
void zero_deltas(struct neural_net *net);
void train(struct neural_net *net, struct training_set *set,
		int epoch_cnt, double learning_rate);
void test_training_set(struct neural_net *net, struct training_set *set);
void dump_neural_net(struct neural_net *net);
void copy_neural_net(struct neural_net *in, struct neural_net **out);
void set_sigmoid(struct neural_net *net);
void set_relu(struct neural_net *net);
#endif
