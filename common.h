#ifndef __COMMON_H__
#define __COMMON_H__

#include <stdlib.h>
#include <string.h>
#include <time.h>

// added for experiments with memory layout on performance

static inline void *alloc(size_t sz) {
	void *ret = malloc(sz);
	memset(ret, 0, sz);
	return ret;
}

static inline void release(void *p) {
	free(p);
}

static inline void init_rand(void) {
	srandom(time(NULL));
}

static inline double drand(void) {
	return (double)random() / (double)RAND_MAX;
}
#endif
