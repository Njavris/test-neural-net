#include "neural_net.h"
#include <stdio.h>
#include <math.h>
#include "common.h"

static double sigmoid(double x) {
	return 1 / (1 + exp(-x));
}

static double dsigmoid(double x) {
//	return sigmoid(x) * (1 - sigmoid(x));
	return x * (1 - x); // since I'm caching values when calculating activations
}

static double relu(double x) {
//	return (x <= 0.0f) ? 0.0f : x;
	return (x < 0.0f) ? 0.00001f : x;	// leaky ReLu for healthy neurons
}

static double drelu(double x) {
//	return (x < 0) ? 0 : 1;
	return (x == 0.0f) ? 0.0f : 1.0f;
}

void shuffle_array(int *array, int size) {
	if (size <= 1)
		return;
	for (int i = 0; i < size / 2; i++) {
		int tmp = array[i];
		int j = random() % size;
		array[i] = array[j];
		array[j] = tmp;
	}
}

struct layer *init_layer(struct layer *prev, struct layer *next, int sz) {
	struct layer *lyr = (struct layer *)alloc(sizeof(struct layer));
	lyr->biases = (double *)alloc(sz * sizeof(double));
	if (prev) {
		lyr->weights =
			(double *)alloc(prev->size * sz * sizeof(double));
	
		for (int i = 0; i < prev->size * sz; i++)
			lyr->weights[i] = drand();
	}
	lyr->tmp = (double *)alloc(sz * sizeof(double));
	lyr->deltas = (double *)alloc(sz * sizeof(double));
	lyr->size = sz;
	if (next) {
		lyr->next = next;
		next->prev = lyr;
	}
	if (prev) {
		lyr->prev = prev;
		prev->next = lyr;
	}
	return lyr;
}

struct neural_net *init_neural_net(int nr_inputs, int nr_outputs,
				int nr_hidden, int hidden_size) {
	struct layer *next = NULL, *prev = NULL;
	struct neural_net *net =
			(struct neural_net *)alloc(sizeof(struct neural_net));
	net->nr_inputs = nr_inputs;
	net->nr_outputs = nr_outputs;
	net->nr_hidden = nr_hidden;
	net->hidden_size = hidden_size;

	init_rand();

	prev = net->input = init_layer(NULL, NULL, nr_inputs);
	for (int i = 0; i < nr_hidden; i ++) {
		next = init_layer(prev, NULL, hidden_size);
		prev->next = next;
		prev = next;
		if (!i)
			net->hidden = next;
	}
	net->output = init_layer(prev, NULL, nr_outputs);
	prev->next = net->output;

	return net;
}

void set_sigmoid(struct neural_net *net) {
	net->activ = sigmoid;
	net->d_activ = dsigmoid;
}

void set_relu(struct neural_net *net) {
	net->activ = relu;
	net->d_activ = drelu;
}

static void release_layer(struct layer *layer) {
	if (layer->next)
		layer->next->prev = layer->prev ? layer->prev : NULL;
	if (layer->prev)
		layer->prev->next = layer->next ? layer->next : NULL;
	release(layer->biases);
	if (layer->weights)
		release(layer->weights);
	release(layer->tmp);
	release(layer->deltas);
	release(layer);
}

static void release_hidden(struct neural_net *net) {
	int count = net->nr_hidden, i = 0;
	struct layer *curr, *next = net->hidden;

	if (!next)
		return;
	do {
		curr = next;
		next = curr->next;
		release_layer(curr);
		i++;
		net->nr_hidden--;
		if (next == net->output) {
			net->hidden = NULL;
			break;
		}
		net->hidden = next;
	} while (curr->next);
	if (i != count)
		printf("freed hidden layer mismatch registered:%d freed:%d\n",
				count, i);
}

void deinit_neural_net(struct neural_net *net) {
	release_hidden(net);
	release_layer(net->input);
	release_layer(net->output);
	release(net);
};

struct training_entry *get_rand_entry(struct training_set *set) {
	return &set->entries[rand() % set->size];
}

struct training_set *init_training_set(struct neural_net *net, int size) {
	struct training_set *set =
		(struct training_set*)alloc(sizeof(struct training_set));
	set->size = size;
	set->entries =
		(struct training_entry*)alloc(sizeof(struct training_entry) *
								set->size);
	for (int i = 0; i < set->size; i++) {
		set->entries[i].inputs =
			(double *)alloc(net->nr_inputs * sizeof(double));
		set->entries[i].outputs =
			(double *)alloc(net->nr_outputs * sizeof(double));
	}
	return set;
}

void deinit_training_set(struct training_set *set) {
	for (int i = 0; i < set->size; i++) {
		release(set->entries[i].inputs);
		release(set->entries[i].outputs);
	}
	release(set->entries);
	release(set);
}

static void calc_layer_activation(struct neural_net *net, struct layer *lyr) {
	struct layer *prev = lyr->prev, *next = lyr->next;
	
	for (int i = 0; i < lyr->size; i++) {
		double act = lyr->biases[i];
		for (int j = 0; j < prev->size; j++)
			act += lyr->weights[i * prev->size + j] *
							prev->tmp[j];
		lyr->tmp[i] = net->activ(act);
	}
	if (next)
		calc_layer_activation(net, next);
}

void calc_activation(struct neural_net *net, double *inputs, int nr_inputs) {
	if (inputs && nr_inputs != net->nr_inputs) {
		printf("Error: Net has %d inputs, but passed %d!!!\n",
					net->nr_inputs, nr_inputs);
		return;
	}
	if (inputs && nr_inputs) {
		for (int i = 0; i < net->nr_inputs; i++)
			net->input->tmp[i] = inputs[i];
	}
	calc_layer_activation(net, net->hidden);
}

double *get_outputs(struct neural_net *net) {
	return net->output->tmp;
}

static void calc_layer_deltas(struct neural_net *net, struct layer *lyr, 
				struct training_entry *val) {
	struct layer *prev = lyr->prev, *next = lyr->next;

	for (int i = 0; i < lyr->size; i++) {
		double delta = 0.0f;
		if (val) { // output layer
			delta = val->outputs[i] - lyr->tmp[i];
		} else { // hidden layer
			for (int j = 0; j < next->size; j++) {
				delta += next->deltas[j] *
					next->weights[j * lyr->size + i];
			}
		}
		lyr->deltas[i] = delta * net->d_activ(lyr->tmp[i]);
	}
	if (prev && prev != net->input)
		calc_layer_deltas(net, prev, NULL);
}

void calc_deltas(struct neural_net *net, double *outputs) {
	struct training_entry entry;
	entry.outputs = outputs;
	calc_layer_deltas(net, net->output, &entry);
}

static void apply_layer_deltas(struct neural_net *net, struct layer *lyr,
				double learning_rate) {
	struct layer *prev = lyr->prev, *next = lyr->next;
	for (int i = 0; i < lyr->size; i++) {
		lyr->biases[i] += lyr->deltas[i] * learning_rate;
		for (int j = 0; j < prev->size; j++)
			lyr->weights[i * prev->size + j] += prev->tmp[j] *
					lyr->deltas[i] * learning_rate;
	}
	if (prev && prev != net->input)
		apply_layer_deltas(net, prev, learning_rate);
}

void apply_deltas(struct neural_net *net, double learning_rate) {
	apply_layer_deltas(net, net->output, learning_rate);
}

static void zero_layer_deltas(struct neural_net *net, struct layer *lyr) {
	for (int i = 0; i < lyr->size; i++)
		lyr->deltas[i] = 0.0f;
	if (lyr->prev && lyr->prev != net->input)
		zero_layer_deltas(net, lyr->prev);
}

void zero_deltas(struct neural_net *net) {
	zero_layer_deltas(net, net->output);
}

void train(struct neural_net *net, struct training_set *set,
				int epoch_cnt, double learning_rate) {
	int *order = (int *)alloc(set->size * sizeof(int));
	for (int i = 0; i < set->size; i++) order[i] = i;

	printf("Training...\n");
	for (int i = 0; i < epoch_cnt; i++) {
		if (i && !(i % 100000))
			printf("%d epochs done\n", i);
		// shuffle for SGD
		shuffle_array(order, set->size);
		for (int j = 0; j < set->size; j++) {
			int it = order[j];
			struct training_entry *entry = &set->entries[it];
			calc_activation(net, entry->inputs, net->nr_inputs);
			zero_layer_deltas(net, net->output);
			calc_layer_deltas(net, net->output, entry);
			apply_layer_deltas(net, net->output, learning_rate);
		}
	}
	release(order);
}

void test_training_set(struct neural_net *net, struct training_set *set) {
	printf("Testing:\n");
	for (int i = 0; i < set->size; i++) {
		struct training_entry *entry = &set->entries[i];
		printf("I: ");
		for (int j = 0; j < net->nr_inputs; j++) 
			printf("%f ", entry->inputs[j]);
		printf("O: ");
		for (int j = 0; j < net->nr_outputs; j++)
			printf("%f ", entry->outputs[j]);

		calc_activation(net, entry->inputs, net->nr_inputs);

		printf("| Calculated: ");
		for (int j = 0; j < net->nr_outputs; j++)
			printf("%f ", net->output->tmp[j]);	
		printf("| Deltas: ");
		for (int j = 0; j < net->nr_outputs; j++)
			printf("%f ", fabs(entry->outputs[j] -
					net->output->tmp[j]));
		printf("\n");
	}
}

static void dump_layers(struct neural_net *net, struct layer *lyr, int it) {
	struct layer *next = lyr->next;

	printf("%d ", it);
	if (net->input == lyr) {	// proly input layer
		for (int i = 0; i < lyr->size; i++)
			printf("I%d\t", i);
	} else {
		struct layer *prev = lyr->prev;
		for (int i = 0; i < lyr->size; i++) {
			printf("b:%f w:[", lyr->biases[i]);
			for (int j = 0; j < prev->size; j++)
				printf("%f,",
					lyr->weights[i * prev->size + j]);
			printf("] ");
		}
	}
	printf("\n");
	
	if (next)
		dump_layers(net, next, ++it);
}

void dump_neural_net(struct neural_net *net) {
	printf("Dump:\n");
	dump_layers(net, net->input, 0);
}

void copy_neural_net(struct neural_net *in, struct neural_net **out) {
	struct neural_net *new = *out;
	struct layer *in_curr, *in_next, *out_curr, *out_next;
	if (!new) {
		new = init_neural_net(in->nr_inputs, in->nr_outputs,
				in->nr_hidden, in->hidden_size);
		new->activ = in->activ;
		new->d_activ = in->d_activ;
	}

	in_next = in->input;
	out_next = new->input;

	while (in_next) {
		in_curr = in_next;
		out_curr = out_next;
		in_next = in_curr->next;
		out_next = out_curr->next;

		memcpy(out_curr->biases, in_curr->biases,
					out_curr->size * sizeof(double));
		if (out_curr->prev) {
			struct layer *pr = out_curr->prev;
			memcpy(out_curr->weights, in_curr->weights,
				pr->size * out_curr->size * sizeof(double));
		}
	}
	*out = new;
}
