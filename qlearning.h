#ifndef __QLEARNING_H__
#define __QLEARNING_H__ 

#include "neural_net.h"
#include <stdint.h>

struct experience {
	int idx;
	int empty;
	double *state0;
	double *state1;
	int screen0;
	int action;
	double reward;
};

struct qlearning {
	struct neural_net *net;
	struct neural_net *target_net;
	struct experience *exp_list;
	int exp_cnt;
	int exp_next;
	int nr_inputs;
	int nr_outputs;

};

struct qlearning *init_qlearning(struct neural_net *net, int input_sz,
					int experiences);
void deinit_qlearning(struct qlearning *qlrn);
int qlearning_max_action(struct qlearning *qlrn, struct neural_net *net);
int qlearning_max(struct qlearning *qlrn, struct neural_net *net);
double *qlearning_get_net_inputs(struct qlearning *qlrn);
double *qlearning_get_target_net_inputs(struct qlearning *qlrn);
void qlearning_duplicate_net(struct qlearning *qlrn);
struct experience *get_next_experience(struct qlearning *qlrn);
struct experience *get_rand_experience(struct qlearning *qlrn);
#endif
