PROJECT:=neuralnet
CC?=gcc
CFLAGS+=-lm -lpthread -lSDL2 -lSDL2main  -O3 
SRC:=main.c neural_net.c breakout.c qlearning.c

all:
	mkdir -p bin
	$(CC) $(CFLAGS) $(SRC) -o bin/$(PROJECT)

clean:
	rm -rf bin


