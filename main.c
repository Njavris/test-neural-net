#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "common.h"
#include "neural_net.h"
#include "breakout.h"
#include "qlearning.h"

void logic_gate_test(int relu) {
	struct training_set *set;
	struct neural_net *net;
	int epochs = 10000;
	double learning_rate = 0.7;
	int inputs = 2;
	int outputs = 1;
	int hidden_layers = 1;
	int neurons_per_layer = 2;

	if (relu)
		learning_rate = 0.1;

	printf("%d epochs with learning rate %f\n", epochs, learning_rate);
	net = init_neural_net(inputs, outputs, hidden_layers,
				neurons_per_layer);
	if (relu)
		set_relu(net);
	else
		set_sigmoid(net);
	set = init_training_set(net, 4);
	for (int i = 0; i < 2; i++)
	for (int j = 0; j < 2; j++) {
		int z = i ^ j;
		set->entries[i * 2 + j].inputs[0] = (double)i;
		set->entries[i * 2 + j].inputs[1] = (double)j;
		set->entries[i * 2 + j].outputs[0] = (double)z;
	}

	for (int i = 0; i < 4; i++)  {
		printf("%f | %f = %f\n", set->entries[i].inputs[0],
						set->entries[i].inputs[1],
						set->entries[i].outputs[0]);
	}

	train(net, set, epochs, learning_rate);
	test_training_set(net, set);
	dump_neural_net(net);

	deinit_training_set(set);
	deinit_neural_net(net);
}

static void breakout(void) {
	struct breakout *brk = init_breakout();
	brk->human_input = 1;
	int score = 0;
	while (1) {
		int ret = breakout_run(brk);
		score = breakout_get_score(brk);
		if (!ret) {
			breakout_reset(brk);
			printf("GAME OVER! Score: %d\n", score);
			score = 0;
		}
		if (ret < 0)
			break;
	}
	deinit_breakout(brk);
}

void deep_q_learn(void) {
	int nr_episodes = 10000000;
	int max_steps_per_episode = 100000;
	int num_experiences = 1000;
	int batch_size = 750;
	int target_net_delay = 1000;
	double learning_rate = 0.01f;
	double gamma = 0.9f;
	double eps_rate = 1.0f;
	double min_eps_rate = 0.01f;
	double max_eps_rate = 1.0f;
	double eps_decay_rate = 0.001f;
	struct experience *curr, *next = NULL;

	struct breakout *brk = init_breakout();
	int screen_sz = breakout_get_screen_sz(brk);
	struct neural_net *net = init_neural_net(screen_sz, 2, 3, 256);
	set_relu(net);
	struct qlearning *qlrn = init_qlearning(net, screen_sz, num_experiences);

	int learn = 1;
	int ep_cnt = 0;
	int frame_cnt = 0;
	int stat_score = 0;
	int ep_frame_cnt = 0;
	int reward_non_zero = 0;
	int reward_zero = 0;
	int nr_batches = 0;
	int target_net_upd = 0;

//	brk->human_input = 1;
	while (learn) {
		int action, direction;
		double eps_rate_threshold = drand();
		curr = next;
		next = get_next_experience(qlrn);
		if (curr) {
			breakout_get_screen(brk, &curr->state1, screen_sz);
			curr->reward = 1;
			curr->empty = 0;
		}
		if (next) {
			breakout_get_screen(brk, &next->state0, screen_sz);
			next->screen0 = 1;
		}


		if (eps_rate_threshold > eps_rate) {
			double *screen = qlearning_get_net_inputs(qlrn);
			breakout_get_screen(brk, &screen, screen_sz);
			action = qlearning_max_action(qlrn, qlrn->net); 
		} else {
			action = random() % 2;
		}
		direction = (!action) ? 1 : -1;
		change_paddle_pos(brk, direction, 0);
		

		next->action = action;
		int ret = breakout_run(brk);

		if (frame_cnt && !(frame_cnt % num_experiences)) {
			struct neural_net *net = qlrn->net;
			struct neural_net *target_net = qlrn->target_net;
			nr_batches ++;
			for (int i = 0; i < batch_size; i++) {
				double *target_outputs, *outputs; 

				struct experience *tmp = get_rand_experience(qlrn);
				if (!tmp)
					continue;

				calc_activation(net, tmp->state0, net->nr_inputs);
				if (tmp->reward) {
					reward_non_zero ++;
					calc_activation(target_net, tmp->state1, target_net->nr_inputs);
				} else {
					reward_zero ++;
				}

				target_outputs = get_outputs(target_net);
				outputs = get_outputs(qlrn->net);
				for (int j = 0; j < target_net->nr_outputs; j++) {
					if (tmp->reward) {
						target_outputs[j] = tmp->reward + gamma * target_outputs[j];
						target_outputs[j] += outputs[j];
						target_outputs[j] /= 2.0;
					} else {
						target_outputs[j] = 0.0f;
					}
				}
				zero_deltas(net);
				calc_deltas(net, target_outputs);
				apply_deltas(net, learning_rate);

				tmp->screen0 = 0;
				tmp->reward = 0;
			}
		}

		if (frame_cnt && !(frame_cnt % target_net_delay)) {
			target_net_upd ++;
			qlearning_duplicate_net(qlrn);
		}

		if (!ret || ep_frame_cnt > max_steps_per_episode) {
			ep_frame_cnt = 0;
			int score = breakout_get_score(brk);
			next->reward = score;
 			breakout_reset(brk);

			eps_rate = min_eps_rate +
				(max_eps_rate - min_eps_rate) *
				exp(-1.0f * eps_decay_rate * ep_cnt);
			ep_cnt ++;
			stat_score += score;

			if (!(ep_cnt % 100)) {
				printf("After %d episodes"
					" 100 ep avg score: %f eps:%f, "
					"Rewards zero: %d non zero: %d "
					"batches: %d target_updates: %d\n",
						ep_cnt,
						(double)stat_score / 1000.0f,
						eps_rate, reward_zero,
						reward_non_zero, nr_batches,
						target_net_upd);
				target_net_upd = 0;
				nr_batches = 0;
				stat_score = 0;
				reward_zero = 0;
				reward_non_zero = 0;
			}
		}

		if (ret < 0)
			learn = 0;
		frame_cnt ++;
		ep_frame_cnt ++;
	}

	deinit_qlearning(qlrn);
	deinit_neural_net(net);
	deinit_breakout(brk);
}

int main(int argc, char **argv) {
//	breakout();
//	logic_gate_test(0);
//	logic_gate_test(1);
	deep_q_learn();
	return 0;
}
