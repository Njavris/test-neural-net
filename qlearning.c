#include "qlearning.h"
#include "common.h"
#include <stdio.h>

struct qlearning *init_qlearning(struct neural_net *net, int input_sz,
					int experiences) {
	struct qlearning *qlrn = (struct qlearning *)
				alloc(sizeof(struct qlearning));
	qlrn->net = net;
	qlrn->nr_inputs = net->nr_inputs;
	qlrn->nr_outputs = net->nr_outputs;
	qlrn->target_net = NULL;
	qlrn->exp_cnt = experiences;
	qlrn->exp_next = 0;
	copy_neural_net(net, &qlrn->target_net);

	qlrn->exp_list = (struct experience *)alloc(
				qlrn->exp_cnt * sizeof(struct experience));
	for (int i = 0; i < experiences; i++) {
		struct experience *exp = &qlrn->exp_list[i];
		exp->idx = i;
		exp->state0 = (double *)alloc(input_sz * sizeof(double));
		exp->state1 = (double *)alloc(input_sz * sizeof(double));
		exp->screen0 = 0;
		exp->empty = 1;
	}

	return qlrn;
}

void deinit_qlearning(struct qlearning *qlrn) {
	release(qlrn->exp_list);
	deinit_neural_net(qlrn->target_net);
	release(qlrn);
}

struct experience *get_next_experience(struct qlearning *qlrn) {
	struct experience *exp = &qlrn->exp_list[qlrn->exp_next];
	qlrn->exp_next ++;
	qlrn->exp_next %= qlrn->exp_cnt;
	return exp;
}

struct experience *get_rand_experience(struct qlearning *qlrn) {
	int rnd = random() % qlrn->exp_cnt;
	for (int i = 0; i < qlrn->exp_cnt; i++) {
		int idx = rnd + i;
		idx %= qlrn->exp_cnt;
		if (!qlrn->exp_list[idx].screen0)
			qlrn->exp_list[idx].empty = 1;

		if (!qlrn->exp_list[idx].empty)
			return &qlrn->exp_list[idx];
	}
	return NULL;
}

int qlearning_max_action(struct qlearning *qlrn, struct neural_net *net) {
	double curr_max_val, *out;;
	int curr_max_idx = 0;

	calc_activation(net, NULL, 0);
	out = get_outputs(net);
	curr_max_val = out[0];
	for (int i = 1; i < net->nr_outputs; i++) {
		if (out[i] > curr_max_val) {
			curr_max_val = out[i];
			curr_max_idx = i;
		}
	}
	return curr_max_idx;
}

int qlearning_max(struct qlearning *qlrn, struct neural_net *net) {
	double curr_max_val, *out;;

	calc_activation(net, NULL, 0);
	out = get_outputs(net);
	curr_max_val = out[0];
	for (int i = 1; i < net->nr_outputs; i++) {
		if (out[i] > curr_max_val) {
			curr_max_val = out[i];
		}
	}
	return curr_max_val;
}

double *qlearning_get_net_inputs(struct qlearning *qlrn) {
	struct neural_net *net = qlrn->net;
	return net->input->tmp;
}

double *qlearning_get_target_net_inputs(struct qlearning *qlrn) {
	struct neural_net *net = qlrn->target_net;
	return net->input->tmp;
}

void qlearning_duplicate_net(struct qlearning *qlrn) {
	copy_neural_net(qlrn->net, &qlrn->target_net);
}
