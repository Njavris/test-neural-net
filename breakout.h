#ifndef __BREAKOUT_H__
#define __BREAKOUT_H__
#include <stdint.h>

struct breakout_screen;

struct block {
	struct block *next;
	int pos_x;
	int pos_y;
	int width;
	int height;
	int draw;
};

struct breakout {
	struct breakout_screen *scr;
	int game_over;
	int ball_pos_x;
	int ball_pos_y;
	int ball_vel_x;
	int ball_vel_y;
	int nr_blocks;
	int score;
	struct block *blck;
	int human_input;
};

struct breakout *init_breakout(void);
int deinit_breakout(struct breakout *breakout);

int breakout_run(struct breakout *brk);
void breakout_reset(struct breakout *brk);
void change_paddle_pos(struct breakout *brk, int delta_x, int delta_y);
int breakout_get_score(struct breakout *brk);
int breakout_get_screen(struct breakout *brk, double **dst, int sz);
int breakout_get_screen_sz(struct breakout *brk);
#endif
